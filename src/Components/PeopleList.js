import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import LazyLoad from 'react-lazyload';

const Peoplelist = ({
  setCharacter,
}) => {
  const [people, setPeople] = useState([])
  const imgURL = 'https://starwars-visualguide.com/assets/img/characters/'

  useEffect(() => {
    fetch('https://swapi.dev/api/people/')
      .then((res) => res.json())
      .then(
        (resp) => {
          setPeople(resp.results)
        },
      )
  }, [])

  function getId(url) {
    return url.split('/')[url.split('/').length - 2]
  }

  return (
    <ul className="listPeoples">
      {people.map((person) => (
        <li className="cardPeople" key={person.name}>
          <LazyLoad height={50}>
            <img className="img" alt={person.name} src={`${imgURL + getId(person.url)}.jpg`} />
          </LazyLoad>
          <div className="desc">
            <h3>{person.name}</h3>
            <p><b>Birth year:</b> {person.birth_year}</p>
            <p><b>Height:</b> {person.height}</p>
            <p><b>Color:</b> {person.skin_color}</p>
          </div>
        </li>
      ))}
    </ul>
  )
}

Peoplelist.propTypes = {
  setCharacter: PropTypes.func.isRequired,
}

export default Peoplelist
