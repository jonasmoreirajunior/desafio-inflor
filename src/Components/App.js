import React, { useState } from 'react'

// Components
import PeopleList from './PeopleList'

// Css
import '../Style/style.css'

export default function App() {
  const [setCharacter] = useState(null)
  return (
    <div className="container">
      <div className="title">
        <h1>Star Wars API</h1>
        <h2>People</h2>
      </div>
      <div>
        <PeopleList setCharacter={setCharacter} />
      </div>
    </div>
  )
}
